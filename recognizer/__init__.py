# /usr/bin/env python3
# -*- coding: utf-8 -*-


import torch


def transcribe(parser, model, audio_frame, decoder, device):
    spect = parser.parse_frame(audio_frame).contiguous()
    spect = spect.view(1, 1, spect.size(0), spect.size(1))
    spect = spect.to(device)
    input_sizes = torch.IntTensor([spect.size(3)]).int()
    out, output_sizes = model(spect, input_sizes)
    decoded_output, decoded_offsets = decoder.decode(out, output_sizes)
    return decoded_output, decoded_offsets
