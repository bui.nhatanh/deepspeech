"""Server-end for the ASR demo."""
import os
import time
import argparse
from time import gmtime, strftime
import socketserver
import struct
import wave
import _init_paths
import torch

from data.data_loader import SpectrogramParser
from helpers.utils import load_model
from models.decoder import GreedyDecoder
from transcribe import transcribe


class AsrTCPServer(socketserver.TCPServer):
    """The ASR TCP Server."""

    def __init__(self,
                 server_address,
                 RequestHandlerClass,
                 speech_save_dir,
                 audio_process_handler,
                 bind_and_activate=True):
        self.speech_save_dir = speech_save_dir
        self.audio_process_handler = audio_process_handler
        socketserver.TCPServer.__init__(
            self, server_address, RequestHandlerClass, bind_and_activate=True)


class AsrRequestHandler(socketserver.BaseRequestHandler):
    """The ASR request handler."""

    def handle(self):
        # receive data through TCP socket
        chunk = self.request.recv(1024)
        target_len = struct.unpack('>i', chunk[:4])[0]
        data = chunk[4:]
        while len(data) < target_len:
            chunk = self.request.recv(1024)
            data += chunk
        # write to file
        filename = self._write_to_file(data)

        print("Received utterance[length=%d] from %s, saved to %s." %
              (len(data), self.client_address[0], filename))
        start_time = time.time()
        transcript = self.server.audio_process_handler(filename)
        finish_time = time.time()
        print("Response Time: %f, Transcript: %s" %
              (finish_time - start_time, transcript))
        self.request.sendall(transcript.encode('utf-8'))

    def _write_to_file(self, data):
        # prepare save dir and filename
        if not os.path.exists(self.server.speech_save_dir):
            os.mkdir(self.server.speech_save_dir)
        timestamp = strftime("%Y%m%d%H%M%S", gmtime())
        out_filename = os.path.join(
            self.server.speech_save_dir,
            timestamp + "_" + self.client_address[0] + ".wav")
        # write to wav file
        file = wave.open(out_filename, 'wb')
        file.setnchannels(1)
        file.setsampwidth(4)
        file.setframerate(16000)
        file.writeframes(data)
        file.close()
        return out_filename


def start_server():
    """Start the ASR server"""
    # prepare data generator
    torch.set_grad_enabled(False)
    device = torch.device("cuda" if args.cuda else "cpu")
    model = load_model(device, args.model_path, args.half)

    if args.decoder == "beam":
        from models.decoder import BeamCTCDecoder

        decoder = BeamCTCDecoder(model.labels, lm_path=args.lm_path, alpha=args.alpha, beta=args.beta,
                                 cutoff_top_n=args.cutoff_top_n, cutoff_prob=args.cutoff_prob,
                                 beam_width=args.beam_width, num_processes=args.lm_workers)
    else:
        decoder = GreedyDecoder(model.labels, blank_index=model.labels.index('_'))

    spect_parser = SpectrogramParser(model.audio_conf, normalize=True)

    # prepare ASR inference handler
    def file_to_transcript(filename):
        transcription, _ = transcribe(audio_path=filename,
                                      spect_parser=spect_parser,
                                      model=model,
                                      decoder=decoder,
                                      device=device,
                                      use_half=args.half)
        return transcription[0]

    # start the server
    server = AsrTCPServer(
        server_address=(args.host_ip, args.host_port),
        RequestHandlerClass=AsrRequestHandler,
        speech_save_dir=args.speech_save_dir,
        audio_process_handler=file_to_transcript)
    print("ASR Server Started.")
    server.serve_forever()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='DeepSpeech transcription server')
    parser.add_argument('--host_ip', type=str, default='0.0.0.0', help="Server's IP address.")
    parser.add_argument('--host_port', type=int, default=8086, help="Server's IP port.")
    parser.add_argument('--cuda', action="store_true", help='Use cuda')
    parser.add_argument('--speech_save_dir', default='tmp/', help="Save folder audio recognizer")
    parser.add_argument('--half', action="store_true",
                        help='Use half precision. This is recommended when using mixed-precision at training time')
    parser.add_argument('--decoder', default="greedy", choices=["greedy", "beam"], type=str, help="Decoder to use")
    parser.add_argument('--model-path', default='pretrained/librispeech_pretrained_v2.pth',
                        help='Path to model file created by training')
    parser.add_argument('--top-paths', default=1, type=int, help='number of beams to return')
    parser.add_argument('--beam-width', default=10, type=int, help='Beam width to use')
    parser.add_argument('--lm-path', default=None, type=str,
                        help='Path to an (optional) kenlm language model for use with beam search (req\'d with trie)')
    parser.add_argument('--alpha', default=0.8, type=float, help='Language model weight')
    parser.add_argument('--beta', default=1, type=float, help='Language model word bonus (all words)')
    parser.add_argument('--cutoff-top-n', default=40, type=int,
                        help='Cutoff number in pruning, only top cutoff_top_n characters with highest probs in '
                             'vocabulary will be used in beam search, default 40.')
    parser.add_argument('--cutoff-prob', default=1.0, type=float,
                        help='Cutoff probability in pruning,default 1.0, no pruning.')
    parser.add_argument('--lm-workers', default=1, type=int, help='Number of LM processes to use')

    # yapf: disable
    args = parser.parse_args()
    start_server()
